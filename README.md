### Particle Swarm Trained Neural Network in C++ ###

This code implements the training of a basic Neural Network (NN) using Particle Swarm Optimization (PSO) in C++. Three NNs are used: A basic XOR NN and two variations of Iris Data Classification.

## Getting started ##
To get started, type "make all" in the default directory. 

## Related Publications ##

[Online](http://www.sciencedirect.com/science/article/pii/S0957417411010086) R. Green, L. Wang, and M. Alam, Training neural networks using Central Force Optimization and Particle Swarm Optimization: Insights and comparisons, Expert Systems with Applications, Volume 39, Issue 1, January 2012, Pages 555-563.