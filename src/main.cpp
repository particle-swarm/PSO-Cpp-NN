#include <iostream>
#include <math.h>
#include <cstdlib>
#include <string>
#include <vector>
#include <fstream>
#include "MersenneTwister.h"
#include "hr_time.h"

using namespace std;
double RealPSO(int , int, int );

typedef vector< vector< vector < long double > > > Array3D;
typedef vector< vector< long double > > Array2D;

double sigMoid(double v){
    return 1/(1+exp(-v));
}

long double sigmoid(long double v){
    return 1/(1+exp(-v));
}

struct Particle {
   vector<double> pos;
   vector<double> vel;
   vector<double> pBest;
   double pBestValue;
};

struct bParticle {
   vector<int> pos;
   vector<double> vel;
   vector<int> pBest;
   double pBestValue;
};

int decode(int j, int k, vector<int> c){
   int sum, x, n;

   sum = 0;
   n = 1;

   for (x = j; x < k; x++) {
       if (c[x] == 1) {
           sum = sum + n;
       }
       n = n * 2;
   }
   return(sum);
}


// 3 variables, 10 bits/variable.
// i is which individual
// Encoded as 30 bits, 10 bits per variable
double f1(vector<int> curSol){
    register int x;
    double sum;

    double solution = 78.6;
    sum = 0.0;

    for (x = 0; x <= 2; x++) {
        sum = sum + pow(((double)(decode((x * 10), (x * 10) + 10, curSol) - 512) / 100.0), 2.0);
    }

    return(solution - sum);
}

double XORNN(vector<double> sol) {
    long double Z=0;
    long double n1,n2,n3,x1,x2, e = 0.0, out;

    vector< vector<long double> > inputs(4, vector<long double>(2,0));
    vector<double> outputs(4);

    inputs[0][0] = 0;inputs[0][1] = 0;outputs[0] = 0;
    inputs[1][0] = 0;inputs[1][1] = 1;outputs[1] = 1;
    inputs[2][0] = 1;inputs[2][1] = 0;outputs[2] = 1;
    inputs[3][0] = 1;inputs[3][1] = 1;outputs[3] = 0;

    //9 dimensions
    for(int x=0; x<4; x++){
        x1 = inputs[x][0]; x2 = inputs[x][1]; out = outputs[x];

        n1 = sigmoid(sol[0]*x1 + sol[1]*x2);
        n2 = sigmoid(sol[2]*x1 + sol[3]*x2);
        n3 = sigmoid(sol[4]*x1 + sol[5]*x2);

        out =  sol[6]*n1 +  sol[7]*n2 +  sol[8]*n3;

        e = e + pow(fabs(e-out),2);
    }

    Z = e/4;
    return -Z;
}

bool loaded = false;
vector< vector<long double > > testData;
vector< vector<long double > > trainData;

double IrisNN(vector<double> sol) {

    long double Z, e=0.0, 
                x1, x2, x3, x4,
                out1, out2, out3, 
                n1, n2, n3, n4, n5;
    
    MTRand mt;

    if(!loaded ){
        ifstream myFile;
        myFile.open("../data/irisData");
        if (myFile.is_open()) {

            for(int x=0; x<150; x++){
                vector<long double> v(5,0);

                myFile >> v[0];
                myFile >> v[1];
                myFile >> v[2];
                myFile >> v[3];

                if(x < 50)          { v[4] = 1; }
                else if(x < 100)    { v[4] = 2; }
                else                { v[4] = 3; }
                
                if(mt.rand() > .33) { trainData.push_back(v); }
                else                { testData.push_back(v);  }
            }
            loaded = true;
            myFile.close();
        }
    }

    for(unsigned int x=0; x<trainData.size(); x++){

        x1 = trainData[x][0]; x2 = trainData[x][1]; x3 = trainData[x][2]; x4 = trainData[x][3];

        n1      = sigmoid(sol[0]*x1  + sol[1]*x2  + sol[2]*x3  + sol[3]*x4);
        n2      = sigmoid(sol[4]*x1  + sol[5]*x2  + sol[6]*x3  + sol[7]*x4);
        n3      = sigmoid(sol[8]*x1  + sol[9]*x2  + sol[10]*x3  + sol[11]*x4);
        out1    = sigmoid(sol[12]*n1 + sol[13]*n2 + sol[14]*n3);
        out2    = sigmoid(sol[15]*n1 + sol[16]*n2 + sol[17]*n3);
        out3    = sigmoid(sol[18]*n1 + sol[19]*n2 + sol[20]*n3);


        if(trainData[x][4] == 1)       { e = e + pow((1-out1) + (0-out2) + (0-out3),2); }
        else if(trainData[x][4] == 2)  { e = e + pow((0-out1) + (1-out2) + (0-out3),2); }
        else                           { e = e + pow((0-out1) + (0-out2) + (1-out3),2); }
    }

    Z = -e/(double)trainData.size();

    return Z;
}

double IrisNN2(vector<double> sol) {

    long double Z, e=0.0, 
                x1, x2, x3, x4,
                out1, out2, out3, 
                n1, n2, n3, n4, n5;
    MTRand mt;

    if(!loaded ){
        ifstream myFile;
        myFile.open("irisData");
        if (myFile.is_open()) {
            for(int x=0; x<150; x++){
                vector<long double> v(5,0);

                myFile >> v[0];
                myFile >> v[1];
                myFile >> v[2];
                myFile >> v[3];
                
                myFile >> v[0];
                myFile >> v[1];
                myFile >> v[2];
                myFile >> v[3];

                if(x < 50)          { v[4] = 1; }
                else if(x < 100)    { v[4] = 2; }
                else                { v[4] = 3; }
                
                if(mt.rand() > .33) { trainData.push_back(v); }
                else                { testData.push_back(v);  }
            }

            loaded = true;
            myFile.close();
        }
    }

    for(int x=0; x<trainData.size(); x++){

        x1 = trainData[x][0]; x2 = trainData[x][1]; x3 = trainData[x][2]; x4 = trainData[x][3];


        n1 = sigmoid(sol[0]*x1 + sol[1]*x2 + sol[2]*x3 + sol[3]*x4);
        n2 = sigmoid(sol[4]*x1 + sol[5]*x2 + sol[6]*x3 + sol[7]*x4);
        n3 = sigmoid(sol[8]*x1 + sol[9]*x2 + sol[10]*x3 + sol[11]*x4);
        n4 = sigmoid(sol[12]*x1 + sol[13]*x2 + sol[14]*x3 + sol[15]*x4);
        n5 = sigmoid(sol[16]*x1 + sol[17]*x2 + sol[18]*x3 + sol[19]*x4);

        out1 = sigmoid(sol[20]*n1 +  sol[21]*n2 +  sol[22]*n3 +  sol[23]*n4 +  sol[24]*n5);
        out2 = sigmoid(sol[25]*n1 +  sol[26]*n2 +  sol[27]*n3 +  sol[28]*n4 +  sol[29]*n5);
        out3 = sigmoid(sol[30]*n1 +  sol[31]*n2 +  sol[32]*n3 +  sol[33]*n4 +  sol[34]*n5);

        if(trainData[x][4] == 1)       { e = e + pow((1-out1) + (0-out2) + (0-out3),2); }
        else if(trainData[x][4] == 2)  { e = e + pow((0-out1) + (1-out2) + (0-out3),2); }
        else                           { e = e + pow((0-out1) + (0-out2) + (1-out3),2); }

    }

    Z = -e/(double)trainData.size();
    return Z;
}

double RealPSO(int popSize, int dim, int iterations, double(*objFunc)(vector<double>), int& numEvals){

    vector<double> gBest(dim,-INFINITY);
    double gBestValue = -INFINITY;
    double result = 0.0;
    MTRand mt;
    double R1 = mt.randExc();
    double DeltaXi = 4/(dim-1);
    double R2 = mt.randExc();
    double C1 = 2;
    double C2 = 2;
    double w = 1;
    double vMax = 6.0;
    double vMin = -6.0;
    double xMax = 2.0, xMin = -2.0;
    int numProbesPerAxis = popSize/dim;
    numEvals = 0;
    
    //Init Pop
    vector<Particle> swarm;
    srand(time(NULL));
    for(int x=0; x<popSize; x++){
        Particle p;
        for(int y=0; y<dim; y++){
            // Uniform on Axis
            //p.pos.push_back(xMin + 0.8 * (xMax-xMin));

            // Uniform on Diagonal
            p.pos.push_back(xMin + x*DeltaXi);

            // Random
            //p.pos.push_back(rand() % (int)xMax + xMin);

            p.vel.push_back(0);
            p.pBestValue = -INFINITY;
        }
        swarm.push_back(p);
    }
    // Uniform on Axis Continued
    /*DeltaXi = (xMax - xMin) / (numProbesPerAxis-1);
    int p;
    for (int i = 0; i < dim; i++) {
        for (int k = 0; k < numProbesPerAxis; k++) {
            p = k + numProbesPerAxis * i;
            swarm[p].pos[i] = xMin + k * DeltaXi;
        }
    }*/


    for(int count=0; count < iterations; count++){

        //Evaluate all individuals
        //for(unsigned int x=0; x<swarm.size(); x++){
        for(unsigned int x=0; x<popSize; x++){
            result = objFunc(swarm[x].pos);
            numEvals++;

            //Local Best
            if(result > swarm[x].pBestValue){
                swarm[x].pBest = swarm[x].pos;
                swarm[x].pBestValue = result;
            }
            //Global Best
            if(result > gBestValue){
                gBest = swarm[x].pos;
                gBestValue = result;
            }
        }

        //Update Positions
        R1 = mt.randExc();
        R2 = mt.randExc();
        for(unsigned int x=0; x< swarm.size(); x++){
            for(int y=0; y< dim; y++){
                double newV = (w*swarm[x].vel[y]+ C1*R1*(swarm[x].pBest[y] - swarm[x].pos[y]) + C2*R2*(gBest[y] - swarm[x].pos[y]));

                /*
                if(newV > vMax){
                    newV = vMax;
                }else if(newV < vMin){
                    newV = vMax;
                }*/
                swarm[x].vel[y] = newV;
                swarm[x].pos[y] += swarm[x].vel[y];
                /*
                if(swarm[x].pos[y] > xMax){
                    swarm[x].pos[y] = xMax;
                }else if(swarm[x].pos[y] < xMin){
                    swarm[x].pos[y] = xMin;
                }*/
            }
        }
        if(fabs(gBestValue)  < 0.02) break;

    }

    // cout << "Number of Evals: " << numEvals << endl;
    // cout << "Best Fitness:    " << gBestValue << endl;
    // cout << endl;
    return gBestValue;
}

int main(){
    double (*rPtr)(vector<double>) = NULL;
    CStopWatch timer;
    double r, totalTime = 0.00, totalFitness = 0.00;
    int numEvals, totalEvals = 0.0;
    int numTrials = 1;

    rPtr = &XORNN;
    totalEvals = totalTime = totalFitness = 0;
    for(int x=0; x<numTrials; x++){
        numEvals = 0;
        
        timer.startTimer();
        r = RealPSO(54, 9, 1000, rPtr, numEvals);
        timer.stopTimer();

        totalTime    += timer.getElapsedTime();
        totalFitness += r;
        totalEvals   += numEvals;
    }

    cout << "************* XOR *************"<< endl;
    cout << "Average Fitness:" << totalFitness/20 << endl;
    cout << "Total Evals:    " << totalEvals << endl;
    cout << "Average Evals:  " << totalEvals/20 << endl;
    cout << "Total Time:     " << totalTime << endl;
    cout << "Average Time:   " << totalTime/20 << endl;
    cout << endl;

    rPtr = &IrisNN;
    totalEvals = totalTime = totalFitness = 0;
    for(int x=0; x<numTrials; x++){
        numEvals = 0;
        
        timer.startTimer();
        r = RealPSO(84, 21, 1000, rPtr, numEvals);
        timer.stopTimer();
        
        totalTime += timer.getElapsedTime();
        totalFitness += r;
        totalEvals += numEvals;
    }

    cout << "************* IrisNN1 *************"<< endl;
    cout << "Average Fitness:" << totalFitness/20 << endl;
    cout << "Total Evals:    " << totalEvals << endl;
    cout << "Average Evals:  " << totalEvals/20 << endl;
    cout << "Total Time:     " << totalTime << endl;
    cout << "Average Time:   " << totalTime/20 << endl;
    cout << endl;

    rPtr = &IrisNN2;
    totalEvals = totalTime = totalFitness = 0;
    for(int x=0; x<numTrials; x++){
        numEvals = 0;

        timer.startTimer();
        r = RealPSO(140, 35, 1000, rPtr, numEvals);
        timer.stopTimer();
        
        totalTime    += timer.getElapsedTime();
        totalFitness += r;
        totalEvals   += numEvals;
    }

    cout << "************* IrisNN2 *************"<< endl;
    cout << "Average Fitness:" << totalFitness/20 << endl;
    cout << "Total Evals:    " << totalEvals << endl;
    cout << "Average Evals:  " << totalEvals/20 << endl;
    cout << "Total Time:     " << totalTime << endl;
    cout << "Average Time:   " << totalTime/20 << endl;
    cout << endl;

    return 0;
}
